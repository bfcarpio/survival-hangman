import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :survival_hangman, SurvivalHangman.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "survival_hangman_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :survival_hangman, SurvivalHangmanWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "45E3j5Ulz7+aIH+hBsT5pc6+Jgg7gfiy5DBj857Kc2ylzkcfSrAQM3HyePpEF8pZ",
  server: false

# In test we don't send emails.
config :survival_hangman, SurvivalHangman.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
