defmodule SurvivalHangmanWeb.PageController do
  use SurvivalHangmanWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
